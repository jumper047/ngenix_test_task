import xml.etree.ElementTree as ET
from concurrent.futures import ProcessPoolExecutor, as_completed
from os import listdir, mkdir, path, remove
from random import randint
from typing import Dict, List, NamedTuple, Tuple
from uuid import uuid4
from zipfile import ZipFile

ZIP_NUM = 50
XML_NUM = 100
VAL_MAX = 100
OBJ_MAX = 10
ZIPNAME_TEMPL = "archive_{}.zip"
XMLNAME_TEMPL = "objdata_{}.xml"
CSV_LVL_NAME = "levels.csv"
CSV_OBJ_NAME = "objects.csv"

zip_dir = path.join(path.curdir, "zips")
csv_dir = path.curdir


class XmlEntry(NamedTuple):
    id: str
    level: str
    objects: List[str]


def create_xml_str() -> str:
    root = ET.Element("root")
    ET.SubElement(root, "var", {"name": "id", "value": str(uuid4())})
    ET.SubElement(root, "var", {"name": "level", "value": str(randint(1, VAL_MAX + 1))})
    objects = ET.SubElement(root, "objects")
    for _ in range(randint(1, OBJ_MAX + 1)):
        ET.SubElement(objects, "object", {"name": str(uuid4())})
    return ET.tostring(root, encoding="unicode")


def create_zip(name: str, data: Dict[str, str]) -> None:
    zf = ZipFile(name, mode="w")
    for filename, xmlstr in data.items():
        zf.writestr(filename, xmlstr)


def extract_var(root: ET.Element, name: str) -> str:
    for elt in root.findall("var"):
        # it's hard to resist temptation to use walrus
        if elt.get("name") == name and ((val := elt.get("value")) is not None):
            return val
    raise ValueError(f'Variable named "{name}" not found.')


def extract_objectnames(root: ET.Element) -> List[str]:
    names = []
    for obj in root.find("objects").findall("object"):
        names.append(obj.get("name"))
    return names


def process_xml_str(string: str) -> Tuple[str, str, List[str]]:
    root = ET.fromstring(string)
    return XmlEntry(
        extract_var(root, "id"), extract_var(root, "level"), extract_objectnames(root)
    )


def process_archive(name: str) -> List[XmlEntry]:
    data = []
    with ZipFile(name) as zf:
        for xmlname in zf.namelist():
            with zf.open(xmlname) as xf:
                data.append(process_xml_str(xf.read().decode()))
    return data


def add_line_to_csv(fname: str, fields: List[str]) -> None:
    with open(fname, "a") as cf:
        cf.write(",".join(fields) + "\n")


if __name__ == "__main__":
    print(f"Creating {ZIP_NUM} zip archives with {XML_NUM} xml files in each zip.")
    if not path.exists(zip_dir):
        mkdir(zip_dir)
    for archnum in range(ZIP_NUM):
        create_zip(
            path.join(zip_dir, ZIPNAME_TEMPL.format(archnum)),
            {XMLNAME_TEMPL.format(n): create_xml_str() for n in range(XML_NUM)},
        )
    print("Part one completed, zip files created. Starting part two.")
    for fname in [CSV_LVL_NAME, CSV_OBJ_NAME]:
        if path.exists(p := path.join(csv_dir, fname)):
            remove(p)
    zipnames = listdir(zip_dir)
    with ProcessPoolExecutor() as executor:
        results = {
            executor.submit(process_archive, path.join(zip_dir, z)): z for z in zipnames
        }
        for r in as_completed(results):
            try:
                data = r.result()
            except Exception as exc:
                print(f"Something went wrong on processing {r}: {exc}")
            else:
                for d in data:
                    add_line_to_csv(CSV_LVL_NAME, [d.id, d.level])
                    for obname in d.objects:
                        add_line_to_csv(CSV_OBJ_NAME, [d.id, obname])
    print(f"Part two completed, {len(results)} zip files were processed.")
    print(f"Extracted object names saved in {path.join(csv_dir, CSV_OBJ_NAME)}")
    print(f"Extracted level values saved in {path.join(csv_dir, CSV_LVL_NAME)}.")
